let mcVersion
const Map = require('./Map').Map

module.exports.server = function (server, { version }) {
  mcVersion = version
}

module.exports.player = async function (player, server) {
  const Item = require('prismarine-item')(mcVersion)
  const itemsByName = require('minecraft-data')(mcVersion).itemsByName

  player.on('move', (position) => {
    if (!player.loadedMap) return

    const data = player.loadedMap.update({
      position
    })

    if (data) player._client.write('map', data)
  })

  player.on('look', ({ yaw }) => {
    if (!player.loadedMap) return

    const data = player.loadedMap.update({
      yaw
    })

    if (data) player._client.write('map', data)
  })

  // todo: create 'holdItem' behavior
  player._client.on('held_item_slot', async ({ slotId } = {}) => {
    const heldItem = player.inventory.slots[36 + slotId]

    if (heldItem === undefined || heldItem.name !== 'filled_map') {
      delete player.loadedMap
      return
    }

    const loadedMap = new Map(heldItem.metadata)
    await loadedMap.load()

    player._client.write('map', loadedMap.itemPacket)

    player.loadedMap = loadedMap
  })

  player.commands.add({
    base: 'map',
    info: 'to map!',
    usage: '/map',
    async action () {
      player.chat('Enjoy mapping the map data!')

      const mapId = 3
      const mapItem = new Item(itemsByName.filled_map.id, 1, mapId)

      player.inventory.updateSlot(36 + player.heldItemSlot, mapItem) // map id 0
    }
  })
}
