const nbt = require('prismarine-nbt')
const fs = require('fs').promises
const path = require('path')

class Map {
  constructor (id, size = 128) {
    this.id = id

    const width = size
    const height = size

    this.data = {
      unlimitedTracking: 0,
      trackingPosition: 1,
      width,
      height,
      scale: 0,
      dimension: 0,
      xCenter: -128,
      zCenter: 0
    }

    this.icons = {
      random1: new MapIcon(5, width - 1, height - 1, 135), // bottom right
      random2: new MapIcon(5, -(width), -(height), 315), // top left
      player: new MapIcon(0, 0, 0, 0)
    }

    this.setColorBuffer(Buffer.alloc(width * height))
    this.setBaseColor(28, 'normal', { x: 64, y: 64 })
  }

  setBaseColor (baseColor, shade = 'normal', location) {
    const shades = {
      normal: 2,
      light: 1,
      lighter: 0,
      lightest: 3
    }

    const mapColor = baseColor * 4 + shades[shade]

    this.setColor(mapColor, location)
  }

  setColor (mapColor, location) {
    const {
      x,
      y
    } = location

    const startLocation = (x - 1) + ((y - 1) * this.data.width)
    const endLocation = (x - 1) + ((y - 1) * this.data.width) + 1

    this.data.colors.fill(mapColor, startLocation, endLocation)
  }

  setColorBuffer (buffer) {
    this.data.colors = buffer
  }

  async load (fileName) {
    const raw = await fs.readFile(fileName || path.join(__dirname, '/test/map_0.dat'))

    const mapData = await new Promise((resolve, reject) => {
      nbt.parse(raw, (error, data) => {
        if (error) reject(error)

        const mapData = nbt.simplify(data).data

        resolve(mapData)
      })
    })

    const colors = new Uint8Array(mapData.colors)

    mapData.colors = Buffer.from(colors)

    this.data = mapData
  }

  get output () {
    return this.data
  }

  get itemPacket () {
    const {
      width,
      height,
      scale,
      colors
    } = this.data

    return {
      itemDamage: this.id, // map id
      scale,
      trackingPosition: true,
      icons: Object.values(this.icons).map(icon => icon.output),
      columns: -(width),
      rows: -(height),
      x: 0,
      z: 0,
      length: colors.length,
      data: colors
    }
  }

  update ({
    location,
    yaw
  }) { // todo: add throttle
    if (yaw) {
      this.icons.player.updateDirection(yaw)
      return this.itemPacket
    }
  }
}

class MapIcon {
  constructor (type, x, y, direction = 0) {
    this.data = {
      direction,
      type,
      x,
      y
    }
  }

  updateDirection (yaw) {
    this.data.direction = this.simplifyDeg(yaw)

    return this.output
  }

  degToSteps (degrees) {
    const directionAccuracy = 360 / 16
    return Math.round(degrees / directionAccuracy)
  }

  simplifyDeg (yaw) {
    return Math.round((yaw % 360) + 360) % 360
  }

  directionAndType (direction, type) {
    if (typeof direction !== 'number' || typeof type !== 'number') return 0

    return parseInt(type.toString(16) + direction.toString(16), 16)
  }

  get output () {
    const {
      direction,
      type,
      x,
      y
    } = this.data

    const directionAndType = this.directionAndType(this.degToSteps(direction), type)

    return {
      directionAndType,
      x,
      y
    }
  }
}

module.exports.Map = Map
module.exports.MapIcon = MapIcon
