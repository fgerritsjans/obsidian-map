# obsidian-map
Map support for flying-squid

## Protocol Docs
- https://wiki.vg/index.php?title=Protocol&oldid=14204#Map
- https://minecraft-data.prismarine.js.org/?v=1.12.2&d=protocol#toClient_map
- https://minecraft.gamepedia.com/index.php?title=Map_item_format&oldid=1182654
